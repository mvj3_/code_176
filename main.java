try {  
           Intent intent = new Intent(  
            RecognizerIntent.ACTION_RECOGNIZE_SPEECH);  
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,  
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);  
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "开启语音");  
  
                startActivityForResult(intent, SPEECHREQUEST);  
    } catch (Exception e) {  
        Toast.makeText(SpeechrecognitionTest.this, e.getMessage(),  
            Toast.LENGTH_LONG).show();  
   }  
如果要对返回的代码进行处理，则需要重写写处理函数，代码如下：  
    @Override  
    protected void onActivityResult(int requestCode, int resultCode,  
            Intent intent) {  
        if (SPEECHREQUEST == requestCode && resultCode == RESULT_OK) {  
            Toast.makeText(SpeechrecognitionTest.this, "返回结果正常",  
                    Toast.LENGTH_LONG).show();  
            ArrayList<String> result = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);  //获取语言的字符   
              
            //如果是页面的话，可以更新到页面  每个字母一列   
//            ListView lv = new ListView(null);   
//               
//            lv.setAdapter(new ArrayAdapter<String>(this, R.layout.main, result));   
              
            String resultString="";  
            //组成字符串   
            for (int i=0;i<result.size();i++){  
                resultString +=result.get(i);  
            }  
        }  
        super.onActivityResult(requestCode, resultCode, intent);  
    }  